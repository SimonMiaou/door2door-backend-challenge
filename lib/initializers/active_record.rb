require 'active_record'
require 'erb'

# The goal of this file is to setup a connection and ensure the tables are present in the database.
# This is just a hacky solution to have something running fast.

# Load configuration
config_file_content = File.read('config/database.yml')
config_hash = YAML.load(ERB.new(config_file_content).result) # rubocop:disable Security/YAMLLoad
config_for_current_environment = config_hash[ENV['RACK_ENV'] || 'development']

# Connect to database and ensure it exists
begin
  ActiveRecord::Base.establish_connection(config_for_current_environment)
  # This is a hack to trigger ActiveRecord::NoDatabaseError if the database doesn't exist
  ActiveRecord::Schema.define { table_exists? :vehicles }
rescue ActiveRecord::NoDatabaseError
  ActiveRecord::Base.establish_connection(config_for_current_environment.merge('database' => 'postgres'))
  ActiveRecord::Base.connection.create_database(config_for_current_environment['database'],
                                                config_for_current_environment)
  ActiveRecord::Base.establish_connection(config_for_current_environment)
end

# Ensure the schema exists
ActiveRecord::Schema.define do
  unless table_exists? :vehicles
    create_table :vehicles, id: false, primary_key: :id do |t|
      t.string :id, primary_key: true
      t.boolean :registered, null: false
    end
  end

  unless table_exists? :locations
    create_table :locations do |t|
      t.string :vehicle_id
      t.decimal :lat, precision: 12, scale: 10
      t.decimal :lng, precision: 12, scale: 10
      t.datetime :at
    end
  end
end
