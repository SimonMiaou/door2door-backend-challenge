require 'initializers/active_record'
require 'models/location'

class Vehicle < ActiveRecord::Base
  has_many :locations
end
