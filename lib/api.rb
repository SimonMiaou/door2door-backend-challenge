require 'sinatra'
require 'vehicles_service'

class API < Sinatra::Base
  configure do
    # The endpoint should use configuration (either a file or environment variable)
    set :vehicles_service, VehiclesService.new(Faye::Client.new('http://localhost:3000/faye'))
  end

  post '/vehicles' do
    request_payload = JSON.parse(request.body.read)
    settings.vehicles_service.register(request_payload['id'])
    status 204
  end

  post '/vehicles/:id/locations' do
    request_payload = JSON.parse(request.body.read)
    settings.vehicles_service.update_location(params['id'],
                                              lat: BigDecimal(request_payload['lat'], 10),
                                              lng: BigDecimal(request_payload['lng'], 10),
                                              at: Time.iso8601(request_payload['at']))
    status 204
  end

  delete '/vehicles/:id' do
    settings.vehicles_service.deregister(params['id'])
    status 204
  end
end
