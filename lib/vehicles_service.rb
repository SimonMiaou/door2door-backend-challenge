require 'faye'
require 'models/vehicle'

class VehiclesService
  def initialize(faye_client = nil)
    @faye_client = faye_client
  end

  def register(vehicle_id)
    vehicle = Vehicle.find_or_initialize_by(id: vehicle_id)
    vehicle.update(registered: true)
  end

  def update_location(vehicle_id, lat:, lng:, at:)
    vehicle = Vehicle.where(registered: true).find(vehicle_id)
    vehicle.locations.create(lat: lat, lng: lng, at: at)

    # Ideally, publishing should happen in a background job, to not impact the database part
    @faye_client&.publish('/locations_updates', 'vehicle_id' => vehicle_id, 'lat' => lat, 'lng' => lng)
  end

  def deregister(vehicle_id)
    vehicle = Vehicle.find(vehicle_id)
    vehicle.update(registered: false)
  end
end
