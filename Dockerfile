FROM alpine

# Install useful package
RUN apk --update add \
  bash \
  build-base \
  curl \
  postgresql-dev \
  ruby \
  ruby-bigdecimal \
  ruby-bundler \
  ruby-dev \
  ruby-irb \
  tmux \
  vim

RUN echo 'gem: --no-rdoc --no-ri' >> /etc/gemrc
RUN gem update --system

# Create the app directory
WORKDIR /app

# Expose port
EXPOSE 3000

# Install gems
ADD Gemfile .
ADD Gemfile.lock .
RUN bundle install

# Copy the app
ADD . .
