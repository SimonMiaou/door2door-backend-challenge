require 'test_helper'

describe API do
  include TestHelpers::Api

  describe 'POST /vehicles' do
    it 'call the service to register the vehicle and return an empty 204' do
      vehicle_id = SecureRandom.uuid

      any_instance_of(VehiclesService) do |klass|
        mock(klass).register(vehicle_id)
      end

      post '/vehicles', { 'id' => vehicle_id }.to_json

      assert_equal 204, last_response.status
      assert_equal '', last_response.body
    end
  end

  describe 'POST /vehicles/:id/locations' do
    it 'call the service to record the vehicle\'s location and return an empty 204' do
      vehicle_id = SecureRandom.uuid
      lat = BigDecimal(rand * 90, 10)
      lng = BigDecimal(rand * 90, 10)
      at = Time.now

      any_instance_of(VehiclesService) do |klass|
        mock(klass).update_location(vehicle_id, lat: lat, lng: lng, at: anything) do |_vehicle_id, options|
          # dates are hard to compare
          assert_equal at.to_i, options[:at].to_i
        end
      end

      post "/vehicles/#{vehicle_id}/locations", { 'lat' => lat, 'lng' => lng, 'at' => at.iso8601 }.to_json

      assert_equal 204, last_response.status
      assert_equal '', last_response.body
    end
  end

  describe 'DELETE /vehicles/:id' do
    it 'call the service to de-register the vehicle and return an empty 204' do
      vehicle_id = SecureRandom.uuid

      any_instance_of(VehiclesService) do |klass|
        mock(klass).deregister(vehicle_id)
      end

      delete "/vehicles/#{vehicle_id}"

      assert_equal 204, last_response.status
      assert_equal '', last_response.body
    end
  end
end
