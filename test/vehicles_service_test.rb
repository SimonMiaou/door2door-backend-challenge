require 'test_helper'

require 'vehicles_service'

describe VehiclesService do
  include TestHelpers::Database

  describe 'register(vehicles_id)' do
    it 'unknown vehicle - create a new entry in the database' do
      id = SecureRandom.uuid

      VehiclesService.new.register(id)

      vehicle = Vehicle.find_by_id(id)
      assert vehicle
      assert vehicle.registered
    end

    it 'known vehicle - set the vehicle to registered' do
      id = SecureRandom.uuid
      vehicle = Vehicle.create!(id: id, registered: false)

      VehiclesService.new.register(id)

      assert vehicle.reload.registered
    end
  end

  describe 'update_location(vehicle_id, lat:, lng:, at:)' do
    describe 'registered vehicle' do
      it 'the location is stored in database' do
        id = SecureRandom.uuid
        vehicle = Vehicle.create!(id: id, registered: true)
        lat = BigDecimal(rand * 90, 10)
        lng = BigDecimal(rand * 90, 10)
        at = Time.now

        VehiclesService.new.update_location(id, lat: lat, lng: lng, at: at)

        location = vehicle.locations.last
        assert_equal lat, location.lat
        assert_equal lng, location.lng
        assert_equal at.to_i, location.at.to_i
      end

      it 'broadcast an event' do
        id = SecureRandom.uuid
        Vehicle.create!(id: id, registered: true)
        lat = BigDecimal(rand * 90, 10)
        lng = BigDecimal(rand * 90, 10)

        faye_client = stub!
        mock(faye_client).publish('/locations_updates', 'vehicle_id' => id, 'lat' => lat, 'lng' => lng)

        VehiclesService.new(faye_client).update_location(id, lat: lat, lng: lng, at: Time.now)
      end
    end

    it 'unregistered vehicle - raise an exception' do
      id = SecureRandom.uuid
      Vehicle.create!(id: id, registered: false)

      assert_raises(ActiveRecord::RecordNotFound) do
        VehiclesService.new.update_location(id, lat: 10.0, lng: 20.0, at: Time.parse('2019-09-01T12:00:00Z'))
      end
    end
  end

  describe 'deregister(vehicles_id)' do
    it 'set the vehicle as not registered' do
      id = SecureRandom.uuid
      vehicle = Vehicle.create!(id: id, registered: true)

      VehiclesService.new.deregister(id)

      refute vehicle.reload.registered
    end
  end
end
