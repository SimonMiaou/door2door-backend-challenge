ENV['RACK_ENV'] = 'test'

require 'api'
require 'byebug'
require 'database_cleaner'
require 'minitest/autorun'
require 'minitest/reporters'
require 'rack/test'
require 'rr'

# Keep a clean database when testing
require 'initializers/active_record'
DatabaseCleaner[:active_record, { connection: :test }]
DatabaseCleaner.clean_with :truncation
DatabaseCleaner.strategy = :transaction

module TestHelpers
  module Api
    extend ActiveSupport::Concern

    included do
      include Rack::Test::Methods
    end

    def app
      API
    end
  end

  module Database
    extend ActiveSupport::Concern

    def setup
      super
      DatabaseCleaner.start
    end

    def teardown
      super
      DatabaseCleaner.clean
    end
  end
end
