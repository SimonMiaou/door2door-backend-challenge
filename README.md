# door2door: Back-end code challenge

https://github.com/door2door-io/fullstack-code-challenge

## Short overview

- `lib/initializers/active_record.rb` is a hacky solution to have active record working
- `lib/models` contain the active record models
- `lib/api.rb` is a sinatra app that implement the 3 endpoint and relay the logic to VehiclesService
- `lib/vehicles_service.rb` is a simple service class to encapsulate the logic

## Run using docker

```bash
# Build docker image
docker-compose build

# Start development environment
docker-compose run --service-ports app
```

## Development

```bash
# Run test suite
bundle exec rake test

# Run linter with auto-correct
bundle exec rubocop --auto-correct

# Start web server
bundle exec thin start

# Subscribe to websocket and output the events
bundle exec rake stream_events
```

## Start the demo

Once in docker, you can run the following snippet. It will:
- Split the screen in two
- Top pane output the events emitted on the websocket
- Bottom pane is the web server
- You can contact the web server using localhost:3000 from your locale machine
- Run the driver simulator: `yarn start localhost:3000`

```bash
tmux split-window -v -p 50
tmux send-keys -t ${window}.0 'rake stream_events' Enter &
tmux send-keys -t ${window}.1 'thin start' Enter &
```
