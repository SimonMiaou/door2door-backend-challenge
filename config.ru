$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), 'lib'))

require 'api'
require 'faye'

use Faye::RackAdapter, mount: '/faye', timeout: 25
run API
